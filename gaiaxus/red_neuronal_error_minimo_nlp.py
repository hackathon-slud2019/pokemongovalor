#!/usr/bin/env python3
import time
import numpy as np
import pandas as pd
from glob import glob
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import dump
import es_core_news_sm
import string
import re
from sklearn.metrics import confusion_matrix

def get_error_minimo():
    errors = []
    for name in glob('datared/*.joblib'):
        # print(name)
        lowerlimit = name.find('___')+3
        upperlimit = name.find('.joblib')
        #print('lowerlimit:', lowerlimit, ', upperlimit:', upperlimit)
        error = name[lowerlimit:upperlimit].replace('_', '.')
        error = float(error)
        errors.append(error)
        # print('error:', error)
    if len(errors) == 0:
        return 1000 # big number
    else:
        return min(errors)


def clean_special_chars(line):
    filter_chars = string.punctuation.replace("|", '').replace('@', '').replace('#', '') + '¿' + '¡'
    chars = re.escape(filter_chars)
    return re.sub(r'[' + chars + ']', '', line)


def clean_url(line):
    new_line = line.replace("|", "|;;").split('|')
    complement = new_line[1] if len(new_line) > 1 else ''
    return re.sub(r'http\S+', '', new_line[0]) + complement.replace(";;", "|")

# X,Y,month,day,FFMC,DMC,DC,ISI,temp,RH,wind,rain,area
df = pd.read_csv('muestraParticipantes_filtered.csv', sep='|')

df['comentario'] = df.apply( lambda row: clean_special_chars(clean_url((row['comentario']))), axis = 1 )
df = df[df['comentario'].str.strip() != ""]

comentario = df['comentario']
normalizado = []

nlp = es_core_news_sm.load()

for n in comentario:
    normalizado.append(nlp(str(n)).vector)

dummiedf = df.loc[:,['polaridad']]
dummiedf = pd.get_dummies(dummiedf)

columns = ['Col'+str(i) for i in range(1, len(normalizado[0])+1)]
X = pd.DataFrame(normalizado, columns=columns)
salida = 'polaridad_Positivo'
y = dummiedf[salida]

#cor=df.corr()
#print(df.corr())

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True) # test_size=0.1

def create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, fxActivacion):
    capas = {
        2: (neuronasCapa1, neuronasCapa2),
        3: (neuronasCapa1, neuronasCapa2, neuronasCapa3),
        4: (neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4),
    }

    mlp = MLPClassifier(
        hidden_layer_sizes=capas[numCapas],
        max_iter=5000000,
        activation=fxActivacion,
        shuffle=True,
        solver='lbfgs'
    )

    mlp.fit(X_train, y_train)

    y_pred = mlp.predict(X)

    cmatrix = confusion_matrix(y, y_pred)

    print('confusion_matrix', cmatrix)

    mse = mean_squared_error(y, y_pred)
    rmse = np.sqrt(mean_squared_error(y, y_pred))
    mae = mean_absolute_error(y, y_pred)
    rmae = np.sqrt(mean_absolute_error(y, y_pred))
    r2 = r2_score(y, y_pred)

    print(u'Error cuadrático medio: {:.10f}'.format(mse))
    print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
    print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
    print(u'Raiz Error absoluto medio: %.2f' % rmae)
    print(u'Estadístico R_2: %.10f' % r2)

    params = mlp.get_params()
    print('Params:', params)
    # print('len coef:', len(mlp.coefs_))
    # print('len coef[0]:', len(mlp.coefs_[0]))
    # print('coefs:', mlp.coefs_)
    # print('len intercepts:', len(mlp.intercepts_))
    # print('len intercepts[0]:', len(mlp.intercepts_[0]))
    # print('intercepts:', len(mlp.intercepts_))

    data = {
        'mse': mse,
        'rmse': rmse,
        'mae': mae,
        'rmae': rmae,
        'r2': r2,
        'params': params,
        'coef': mlp.coefs_,
        'intercepts': mlp.intercepts_,
        'y_pred': y_pred
    }
    return rmse, data, mlp

error_obtenido = get_error_minimo()
data = None
mlp = None
while True:
    numCapas = randint(2, 4)
    # https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw
    neuronasCapa1 = randint(9, 100)
    neuronasCapa2 = randint(1, 50)
    neuronasCapa3 = randint(1, 20)
    neuronasCapa4 = randint(1, 10)
    indexActivation = randint(0, 3)
    activacion = ('identity', 'logistic', 'tanh', 'relu')
    error_obtenido, data, mlp = create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, activacion[indexActivation])
    if error_obtenido < get_error_minimo():
        print('Datos:', data)
        print('error_obtenido', error_obtenido)
        texto_error_obtenido = '{0:f}'.format(error_obtenido).replace('.', '_')
        millis = int(round(time.time() * 1000))
        dump(mlp, 'datared/MLPClassifier__{}__{}___{}.joblib'.format(salida, millis, texto_error_obtenido))
        error_minimo = error_obtenido
        print('Encontre un mínimo', error_minimo)