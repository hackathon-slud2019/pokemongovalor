#!/usr/bin/env python3
import pandas as pd
from joblib import load

df = pd.read_csv('muestraParticipantes_filter_all_data.csv', sep='|')
data_corpus = df['Comentarios'].values
print(data_corpus)
vectorizer = load('data/vectorizer___1567262666049_new.joblib')
X= vectorizer.transform(data_corpus)
array = X.toarray()[0]
for i in array:
    if i == 1:
        print(i)

vocabulary = load('data/vocabulary___1567262666049_new.joblib')
print(vocabulary.get_feature_names())
