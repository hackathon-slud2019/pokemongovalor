#!/usr/bin/env python3
# https://www.guru99.com/word-embedding-word2vec.html
import time
import pandas as pd
from joblib import dump
from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer()
df = pd.read_csv('muestraParticipantes_filter_all_data.csv', sep='|')

data_corpus = df['Comentarios'].values

vocabulary = vectorizer.fit(data_corpus)

millis = int(round(time.time() * 1000))
dump(vectorizer, 'data/vectorizer___{}_new.joblib'.format(millis))
dump(vocabulary, 'data/vocabulary___{}_new.joblib'.format(millis))

X = vectorizer.transform(data_corpus)
print(X.toarray(), len(X.toarray()[0]))
print(vocabulary.get_feature_names())
