#!/usr/bin/env python3
import pandas as pd
from joblib import load

df = pd.read_csv('muestraParticipantes_filtered.csv', sep='|')
data_corpus = df['comentario'].values
print(data_corpus)
vectorizer = load('data/vectorizer___1567225831886.joblib')
X= vectorizer.transform(data_corpus)
array = X.toarray()[0]
for i in array:
    if i == 1:
        print(i)

vocabulary = load('data/vocabulary___1567225831886.joblib')
print(vocabulary.get_feature_names())
