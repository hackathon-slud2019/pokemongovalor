#!/usr/bin/env python3
import time
import numpy as np
import pandas as pd
from glob import glob
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import dump, load
import es_core_news_sm
import string
import re
from sklearn.metrics import confusion_matrix

def get_score():
    scores = []
    for name in glob('datared/*.joblib'):
        # print(name)
        lowerlimit = name.find('___')+3
        upperlimit = name.find('.joblib')
        #print('lowerlimit:', lowerlimit, ', upperlimit:', upperlimit)
        score = name[lowerlimit:upperlimit].replace('_', '.')
        score = float(score)
        scores.append(score)
        # print('error:', error)
    if len(scores) == 0:
        return 0 # min score
    else:
        return max(scores)
print('initial get_score()', get_score())

def clean_special_chars(line):
    filter_chars = string.punctuation.replace("|", '').replace('@', '').replace('#', '') + '¿' + '¡'
    chars = re.escape(filter_chars)
    return re.sub(r'[' + chars + ']', '', line)


def clean_url(line):
    new_line = line.replace("|", "|;;").split('|')
    complement = new_line[1] if len(new_line) > 1 else ''
    return re.sub(r'http\S+', '', new_line[0]) + complement.replace(";;", "|")

# X,Y,month,day,FFMC,DMC,DC,ISI,temp,RH,wind,rain,area
df = pd.read_csv('muestraParticipantes_filtered.csv', sep='|')

#df['comentario'] = df.apply( lambda row: clean_special_chars(clean_url((row['comentario']))), axis = 1 )
df = df[df['comentario'].str.strip() != ""]

comentario = df['comentario']
normalizado = []

nlp = es_core_news_sm.load()

for n in comentario:
    normalizado.append(nlp(str(n)).vector)

dummiedf = df.loc[:,['polaridad']]
dummiedf = pd.get_dummies(dummiedf)

columns = ['Col'+str(i) for i in range(1, len(normalizado[0])+1)]
X = pd.DataFrame(normalizado, columns=columns)
y = dummiedf

def recall(tp, fp, fn, tn):
    return tp/(tp+fn)

def precision(tp, fp, fn, tn):
    return tp/(tp+fp)

def fmeasure(cmatrix):
    tp, fp, fn, tn = get_values(cmatrix)
    _recall = recall(tp, fp, fn, tn)
    _precision = precision(tp, fp, fn, tn)
    return (2*_recall*_precision) / (_recall+_precision)

def get_values(cmatrix):
    tp = cmatrix[0][0]
    fp = cmatrix[0][1]
    fn = cmatrix[1][0]
    tn = cmatrix[1][1]
    return tp, fp, fn, tn

def print_error(y, y_pred):
    mse = mean_squared_error(y, y_pred)
    rmse = np.sqrt(mean_squared_error(y, y_pred))
    mae = mean_absolute_error(y, y_pred)
    rmae = np.sqrt(mean_absolute_error(y, y_pred))
    r2 = r2_score(y, y_pred)

    print(u'Error cuadrático medio: {:.10f}'.format(mse))
    print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
    print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
    print(u'Raiz Error absoluto medio: %.2f' % rmae)
    print(u'Estadístico R_2: %.10f' % r2)
#cor=df.corr()
#print(df.corr())

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True) # test_size=0.1

def create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, fxActivacion):
    capas = {
        2: (neuronasCapa1, neuronasCapa2),
        3: (neuronasCapa1, neuronasCapa2, neuronasCapa3),
        4: (neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4),
    }

    mlp = MLPClassifier(
        hidden_layer_sizes=capas[numCapas],
        max_iter=5000000,
        activation=fxActivacion,
        shuffle=True,
        solver='lbfgs'
    )
    mlp.fit(X_train, y_train)

    # dump(mlp, './data/prueba.joblib2')
    # mlp = load('./data/prueba.joblib2')

    y_pred = mlp.predict(X)

    y2 = y.values.transpose()
    y_pred2 = y_pred.transpose()

    # negativo, neutro, positivo
    cmatrix_negativo = confusion_matrix(y2[0], y_pred2[0])
    cmatrix_neutro = confusion_matrix(y2[1], y_pred2[1])
    cmatrix_positivo = confusion_matrix(y2[2], y_pred2[2])

    print('cmatrix_negativo', cmatrix_negativo)
    print('cmatrix_neutro', cmatrix_neutro)
    print('cmatrix_positivo', cmatrix_positivo)

    score_negativo = fmeasure(cmatrix_negativo)
    score_neutro = fmeasure(cmatrix_neutro)
    score_positivo = fmeasure(cmatrix_positivo)

    print('score_negativo', score_negativo)
    print('score_neutro', score_neutro)
    print('score_positivo', score_positivo)

    score_all = score_negativo + score_neutro + score_positivo
    print('score_all', score_all)

    print('error negativo:')
    print_error(y2[0], y_pred2[0])

    print('error neutro:')
    print_error(y2[1], y_pred2[1])

    print('error positivo:')
    print_error(y2[2], y_pred2[2])

    params = mlp.get_params()
    print('Params:', params)
    # print('len coef:', len(mlp.coefs_))
    # print('len coef[0]:', len(mlp.coefs_[0]))
    # print('coefs:', mlp.coefs_)
    # print('len intercepts:', len(mlp.intercepts_))
    # print('len intercepts[0]:', len(mlp.intercepts_[0]))
    # print('intercepts:', len(mlp.intercepts_))
    data = {
        'score': score_all,
        'params': params,
        'coef': mlp.coefs_,
        'intercepts': mlp.intercepts_,
        'y_pred': y_pred
    }
    return score_all, data, mlp

data = None
mlp = None
while True:
    numCapas = randint(2, 4)
    # https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw
    neuronasCapa1 = randint(9, 100)
    neuronasCapa2 = randint(1, 50)
    neuronasCapa3 = randint(1, 20)
    neuronasCapa4 = randint(1, 10)
    indexActivation = randint(0, 3)
    activacion = ('identity', 'logistic', 'tanh', 'relu')
    score, data, mlp = create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, activacion[indexActivation])
    if score > get_score():
        print('Datos: ', data)
        print('score: ', score)
        texto_score_obtenido = '{0:f}'.format(score).replace('.', '_')
        millis = int(round(time.time() * 1000))
        dump(mlp, 'datared/MLPClassifier__all__{}___{}.joblib'.format(millis, texto_score_obtenido))
        print('Encontre un score maximo', score)