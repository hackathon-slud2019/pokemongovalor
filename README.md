## Inteligencia Artificial con Maching Learning para categorizar datos de Twitter SLUD 2019
___

Este proyecto se realizo bajo GNU GENERAL PUBLIC LICENSE [LICENSE can be found here](http://choosealicense.com/licenses/gpl-3.0/)

Puedes encontrar el entrenamiento de una red neuronal de tipo perceptrón multicapa (MLP) es una clase de red neuronal artificial de alimentación directa.

Pasos del desarrollo:
&nbsp;
![](./assets/redes.png)
- Normalización de los datos.
- Entrenamiento y configuración de la red neuronal.
- Creación del modelo de la red Neuronal para Categorizar mensajes de twitter
&nbsp;
![](./assets/vistas.png)
&nbsp;
- Se utiliza el modelo de la red Neuronal.
- Creación servicio REST en DJANGO [webservice](https://gitlab.com/hackathon-slud2019/pokemongovalor/tree/master/webservice)
- Creación de vista en Angular 7 [frontend-twitter](https://gitlab.com/hackathon-slud2019/pokemongovalor/tree/master/frontend-twitter)
  
Tambien puedes encontrar un bot en telegram el cual utiliza el servicio REST, para ejecutarlo tienes que seguir los siguientes pasos:

- Ingresar a la carpeta Pytucharmander y ejecutar el script de Python que lleva como nombre: *bot_telegram.py*
- Una vez ejecutado ingresar a telegram y buscar el bot SLUD2019
- En este punto el bot te indicara los pasos a seguir, para poder analizar las cadenas de caracteres que desee, ya sea *Twitter* o *Personal*.

&nbsp;
![](./assets/img/telegram.png)
&nbsp;

Nuestra presentación la cual se realizo con markdown y se encuentra desplegada [hackathon-slud2019](https://gitpitch.com/hackathon-slud2019/pokemongovalor?grs=gitlab#/)

<div class="iframe_container">
  <iframe width="560" height="315" src="https://gitpitch.com/hackathon-slud2019/pokemongovalor?grs=gitlab#/" frameborder="0" allowfullscreen></iframe>
</div>