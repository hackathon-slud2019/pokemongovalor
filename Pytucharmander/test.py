import numpy as np
import pandas as pd
from joblib import load
import spacy
from sklearn.metrics import confusion_matrix
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

nlp = spacy.load('es_core_news_sm')

df = pd.read_csv('/home/shade/slud2019/pokemongovalor/gaiaxus/muestraParticipantes_filtered.csv', sep='|')

coment =df['polaridad']
normalizado =[]
for n in coment:
    normalizado.append(nlp(str(n)).vector)

X = pd.DataFrame(normalizado)

mlp_negative = load('/home/shade/slud2019/pokemongovalor/gaiaxus/data/MLPClassifier__1567274389890___0_059648_new.joblib')

y_pred_negative = mlp_negative.predict(X)
print('y_pred_negative', y_pred_negative)

dummiedf = df.loc[:,['polaridad']]
dummiedf = pd.get_dummies(dummiedf)

y = dummiedf

y_pred = mlp_negative.predict(X)

y = y.values.transpose()
y_pred = y_pred.transpose()

cmatrix_a = confusion_matrix(y[0], y_pred[1])
print('confusion_matrix', cmatrix_a)
cmatrix_b = confusion_matrix(y[0], y_pred[2])
print('confusion_matrix', cmatrix_b)
cmatrix_c = confusion_matrix(y[1], y_pred[2])
print('confusion_matrix', cmatrix_c)


mse = mean_squared_error(y, y_pred)
rmse = np.sqrt(mean_squared_error(y, y_pred))
mae = mean_absolute_error(y, y_pred)
rmae = np.sqrt(mean_absolute_error(y, y_pred))
r2 = r2_score(y, y_pred)

print(u'Error cuadrático medio: {:.10f}'.format(mse))
print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
print(u'Raiz Error absoluto medio: %.2f' % rmae)
print(u'Estadístico R_2: %.10f' % r2)