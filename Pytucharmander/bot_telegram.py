import logging
import tweepy
import requests 
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import ConversationHandler, CallbackQueryHandler
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

updater = Updater(token='979052162:AAHkG6nWen82IxhqPxnc3N2C-F6u-umT9xk', use_context=True)
dispatcher = updater.dispatcher

logger = logging.getLogger(__name__)
MESSAGE, MESSAGETWO = range(2)

consumer_key = "LtFTlRSOAeMutYzbPMRRQgDm8"
consumer_secret = "IUQYh3X18uHIigO2qjYUYR26C2ZYQKlgW33QmwA4GEvPPTapBy"

URL = 'http://192.168.31.213:8080/datos_twetter/'
URL2 = 'http://192.168.31.213:8080/datos_compuestos/'

def start(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text="Yo soy un bot, Por favor, ingresa tu mensaje seguido de /twitter o /personal. \n Gracias",
                                reply_markup=ReplyKeyboardRemove())

def twitter(update, context):
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    params = {'hastag': str(update.message.text)}
    r = requests.post(url = URL, json = params) 
    for n in r.json():
        update.message.reply_text(n,reply_markup=ReplyKeyboardRemove())

def personal(update, context):
    user = update.message.from_user
    logger.info("Gender of %s: %s", user.first_name, update.message.text)
    param = {'Comentarios': str(update.message.text)}
    r = requests.post(url = URL2, json = param)
    update.message.reply_text(r.json(),reply_markup=ReplyKeyboardRemove())

def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

start_handler = CommandHandler('start', start)
twitter = CommandHandler('twitter', twitter)
personal = CommandHandler('personal', personal)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)


dispatcher.add_handler(start_handler)
dispatcher.add_handler(twitter)
dispatcher.add_handler(personal)

updater.start_polling()
