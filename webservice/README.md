## Web service para categorizar datos de Twitter SLUD 2019
___

### Creación entorno virtual

```bash
virtualenv env
#Activar entorno virtual
source ./env/bin/active
```

### Instalar dependencias

```bash
pip3 install -r requirements
```

### Ejecutar aplicación

```bash
python3 manage.py runserver 0.0.0.0:8000
```

