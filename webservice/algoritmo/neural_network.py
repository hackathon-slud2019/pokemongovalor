import numpy as np
import pandas as pd
from joblib import load
import spacy

nlp = spacy.load('es_core_news_sm')
#mlp_negative = load('./algoritmo/resources/databest/MLPClassifier__polaridad_Negativo__1567235892745___0_270275.joblib')
#mlp_neutral= load('./algoritmo/resources/databest/MLPClassifier__polaridad_Neutral__1567239735871___0_270275.joblib')
#mlp_positive = load('./algoritmo/resources/databest/MLPClassifier__polaridad_Positivo__1567253784687___0_018264.joblib')
modelo = load('./algoritmo/resources/databest/MLPClassifier__all__1567290075297___2_798537.joblib')
def neural_network(twitte):
    print('Mensaje de t: ',twitte['Comentarios']) 
    X = pd.DataFrame([nlp(twitte['Comentarios']).vector])
    #y_pred_negative = mlp_negative.predict(X)
    #y_pred_neutral= mlp_negative.predict(X)
    #y_pred_positive = mlp_positive.predict(X)
    prediccionAll = modelo.predict(X)
    print('predic red neuronal : ',prediccionAll)
    #polaridad_Negativo  polaridad_Neutral  polaridad_Positivo
    y_pred_negative = prediccionAll[0][0]
    y_pred_neutral= prediccionAll[0][1]
    y_pred_positive = prediccionAll[0][2]
    prediccion={'Negative': y_pred_negative, 'Neutral': y_pred_neutral, 'Positive': y_pred_positive}
    #prediccion={'Negative': y_pred_negative[0], 'Neutral': y_pred_neutral[0], 'Positive': y_pred_positive[0]}
    return prediccion

def neural_network_tweets(twitte):
    print("Mensaje que llega a la red neuronal: ", twitte)
    mensajes=[]
    for i in twitte:
        X=pd.DataFrame([nlp(i).vector])
        #y_pred_negative = mlp_negative.predict(X)
        #y_pred_neutral= mlp_negative.predict(X)
        #y_pred_positive = mlp_positive.predict(X)
        prediccionAll = modelo.predict(X)        
        y_pred_negative = prediccionAll[0][0]
        y_pred_neutral= prediccionAll[0][1]
        y_pred_positive = prediccionAll[0][2]
        mensajes.append({
             'Mensaje':i,
             'Negative':y_pred_negative,
             'Neutral':y_pred_neutral,
             'Positive':y_pred_positive
             })
        # mensajes.append({
        #      'Mensaje':i,
        #      'Negative':y_pred_negative[0],
        #      'Neutral':y_pred_neutral[0],
        #      'Positive':y_pred_positive[0]
        #      })

    return mensajes