from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from algoritmo.neural_network import neural_network,neural_network_tweets
from django.shortcuts import render
import math
import tweepy
import re
import string
# from _config import *
# Create your views here.

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def clean_special_chars(line):
    filter_chars = string.punctuation.replace("|", '').replace('@', '').replace('#', '') + '¿' + '¡'
    chars = re.escape(filter_chars)
    return re.sub(r'[' + chars + ']', '', line)

def clean_url(line):
    new_line = line.replace("|", "|;;").split('|')
    complement = new_line[1] if len(new_line) > 1 else ''
    return re.sub(r'http\S+', '', new_line[0]) + complement.replace(";;", "|")


def cleanTweet(text):
    texto = clean_special_chars(clean_url(text))
    return texto

def response_options():
    response = HttpResponse(status=200)
    response['Allow'] = 'OPTIONS, GET, POST'
    response['Access-Control-Request-Method'] = 'OPTIONS, GET, POST'
    response['Access-Control-Request-Headers'] = 'Content-Type'
    response['Access-Control-Allow-Headers'] = 'Content-Type'
    return response

def response_cors(response):
    response['Access-Control-Allow-Origin'] = '*'
    return response

@csrf_exempt
def datos_compuesto_list(request):
    """
    """
    if request.method == 'GET':
        return response_cors(HttpResponse(status=501))

    elif request.method == 'POST':
        datos = JSONParser().parse(request)
        polaridad = neural_network(datos)
        print(polaridad)
        # serializer_medida.pop('senal_transmision')
        # serializer_medida.pop('senal_reflexion')
        response = [{           
           'polaridad_Negativo':polaridad['Negative'],
            'polaridad_Neutral':polaridad['Neutral'],
            'polaridad_Positivo':polaridad['Positive'] 
        }]
        return response_cors(JSONResponse(response, status=200))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())
@csrf_exempt
def twetter(request):
    consumer_key = "LtFTlRSOAeMutYzbPMRRQgDm8"
    consumer_secret = "IUQYh3X18uHIigO2qjYUYR26C2ZYQKlgW33QmwA4GEvPPTapBy"
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    api = tweepy.API(auth)
    if request.method == 'GET':
        return response_cors(HttpResponse(status=501))

    elif request.method == 'POST':
        datos = JSONParser().parse(request)
        allTweet=[]
        print('dato que llega: ',datos['hastag'])
        for tweet in tweepy.Cursor(api.search, q=datos['hastag']).items(10):
            lineTweet=cleanTweet(tweet.text)
            #lineTweet =tweet.text
            allTweet.append(lineTweet)
        polaridad = neural_network_tweets(allTweet)
        print(polaridad)
        response = []
        for i in polaridad:
            response.append(
                {
                    'tweet':i['Mensaje'],            
                    'polaridad_Negativo':i['Negative'],
                    'polaridad_Neutral':i['Neutral'],
                    'polaridad_Positivo':i['Positive']  
                }
            )
        # serializer_medida.pop('senal_transmision')
        # serializer_medida.pop('senal_reflexion')
        return response_cors(JSONResponse(response, status=200))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())




