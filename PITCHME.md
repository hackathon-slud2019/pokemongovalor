## Inteligencia Artificial con Maching Learning para categorizar datos de Twitter SLUD 2019

---

### Analisis descriptivo de los datos

---?color=linear-gradient(180deg, white 75%, black 25%)
@title[Categorización de los datos]

@snap[west span-50 text-08]
Al realizar un primer acercamiento a los datos se logra determinar que son de tipo categoricos ó cualitativos, dado que se lográn diferenciar facilmente las categorias en las que estos estan divididos.
@snapend

@snap[east span-45]
### Los datos son: 
Categoricos Nominales
@snapend

@snap[south span-100 text-white text-05]
Se determina que los datos ademas de categoricos son nominales, es porqué presentan mas de dos categorias y eso hace que no sean dicotomos. Ademas no existe un orden natural entre las categorias y eso descarta que sean categoricos ordinales.
@snapend

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)
@title[Presentación de datos categoricos]

@snap[north]
Tabla de frecuencia
@snapend

<table>
  <tr>
    <th>X<sub>0</sub></th>
    <th>f<sub>i</sub></th>
    <th>f<sub>r</sub></th>
    <th>F<sub>i</sub></th>
    <th>F<sub>r</sub></th>
  </tr>
  <tr>
    <td>Neutral</td>
    <td>2053</td>
    <td>68.478986</td>
    <td>2053</td>logo : assets/img/logo.png
    <td>68.478986</td>
  </tr>
  <tr class="fragment">
    <td>Negativo</td>
    <td>913</td>
    <td>30.453636</td>
    <td>2966</td>
    <td>98.932622</td>
  </tr>
  <tr class="fragment">
    <td>Positivo</td>
    <td>32</td>
    <td>1.067378</td>
    <td>2998</td>
    <td>100</td>
  </tr>
</table>

@snap[south-west span-50 text-black text-05]
X<sub>0</sub>	= Variable ordenada.
f<sub>i</sub>	= Frecuencia absoluta.
f<sub>r</sub>	= Frecuencia relativa.
@snapend

@snap[south-east span-50 text-black text-05]
F<sub>i</sub>	= Frecuencia absoluta acumulada.
F<sub>r</sub>	= Frecuencia relativa acumulada.
@snapend

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[Grafico de barras]
### Grafico de barras de las categorias de los datos

---?image=assets/img/grafico_barras.png&size=40%

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[Grafico frecuencua acumulada]
### Grafico frecuencia acumulada de las categorias de los datos

---?image=assets/img/grafico_frecuencia_acumulada.png&size=40%

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[Grafico de barras]
### Grafico circular de las categorias de los datos

---?image=assets/img/grafico_circular.png&size=40%

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[north span-100]
### Entrenamiento del los modelos separados
@snapend

@snap[midpoint span-100 text-08]
Se toma la decisión de entrenar tres (3) redes neuronales, por cada una de las variables, por: 
1. Facilidad de calculo.
2. Menor tiempo de entrenamiento de la red neuronal.
3. Mayor tiempo para la ejecución de analisís.
@snapend

@snap[south span-100 text-black text-06]
La separación de los modelos demostro, que el entrenar redes separadas si es mas rapido, las redes con pocos datos, no generan suficiente entropia ergo el modelo no resulta ser lo suficientemente confiable.
@snapend

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[north span-100]
### Resultados de los modelos separados
@snapend

@snap[midpoint span-100 text-08]
Cuando se  le solicita a la red que evalue datos que se encuentran dentro del espacio muestral, se inclina hacia los datos con mayor ponderancia en la muestra, en cambio cuando se proporciona información que no esta relacionada con la muestra, la tendencia varia, esto conlleva a que la información se incline hacia la red con menor cantidad de datos de entrenamiento.
@snapend

@snap[south span-100 text-black text-06]
Las redes con un espacio muestral cargado no proporcionan los mejores resultados.
@snapend

---?color=linear-gradient(180deg, #5384AD 75%, white 25%)

@snap[midpoint span-100]
### evaluación del modelo
@snapend
---?image=assets/img/eval-model1.png&size=50%
