import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  url = environment.url
  constructor(private httpClient: HttpClient) {
  }
  sendMesseger(data: any) {
    return this.httpClient.post(this.url + '/datos_compuestos/', data)
  }
  GetTwitter(data:any){
    return this.httpClient.post(this.url+'/datos_twetter/', data)
  }
}
