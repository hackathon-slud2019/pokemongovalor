import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormularioComponent } from './pages/formulario/formulario.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'twitter',
    pathMatch: 'full'
  },
  {
    path: 'twitter',
    component: FormularioComponent,
    data: {
      title: 'Twitter'
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
