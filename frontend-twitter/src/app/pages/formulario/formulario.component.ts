import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
  tab = 1;
  sentenceOne = '';
  sentence = '';
  prediccion: any;
  prediccionTwets: any = [];
  constructor(
    private service: ServicesService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
  }
  evaluarMensaje() {
    let data = {
      'Comentarios': this.sentenceOne
    }
    this.service.sendMesseger(data).subscribe(
      data => {
        this.prediccion = data;
        this.toastr.success('Se a realizado la clasificación');
      },
      error =>{
        this.toastr.info("No a podido encontrar la predicción")
      }
    )
  }
  hastagTwitter() {
    let data = {
      'hastag': this.sentence
    }
    this.service.GetTwitter(data).subscribe(
      data => {
        this.prediccionTwets = data
        console.log(this.prediccionTwets);
        this.toastr.success('Se a realizado la clasificación');
      },
      error => {
        this.toastr.info("No a podido encontrar la predicción", "Revisa la conexión a internet")
      }
    );
  }
}
