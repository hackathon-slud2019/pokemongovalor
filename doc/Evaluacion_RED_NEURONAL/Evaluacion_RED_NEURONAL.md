
# Hackaton Inteligencia Artificial SLUD2019
## Grupo: PokemonGoValor
### Participantes:

* Jorge Iluses Useche
* Fernando Pineda
* Andrés Mauricio Acosta
+ Jhon Alexander Galindo Ambuila


```python
import time
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import load
from matplotlib.pyplot import figure
from sklearn.metrics import confusion_matrix
```


```python
df=pd.read_csv('../muestraParticipantes_filtered.csv','|')
df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>comentario</th>
      <th>polaridad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>2998</td>
      <td>2998</td>
    </tr>
    <tr>
      <th>unique</th>
      <td>2998</td>
      <td>3</td>
    </tr>
    <tr>
      <th>top</th>
      <td>Volviendo heterosexual a Claudia López. https:...</td>
      <td>Neutral</td>
    </tr>
    <tr>
      <th>freq</th>
      <td>1</td>
      <td>2053</td>
    </tr>
  </tbody>
</table>
</div>




```python
plt.figure(figsize=(9, 8), dpi= 160, facecolor='w', edgecolor='k')
plt.plot(df['polaridad'], color='r')
```




    [<matplotlib.lines.Line2D at 0x7f40f5aa1278>]




![png](output_3_1.png)



```python
y = df['polaridad']
y.describe()
```




    count        2998
    unique          3
    top       Neutral
    freq         2053
    Name: polaridad, dtype: object




```python
pd.value_counts(df['polaridad'])
100 * df['polaridad'].value_counts() / len(df['polaridad'])
plot = df['polaridad'].value_counts().plot(kind='bar', title='Reacciones Twittes', figsize=(12, 12))
```


![png](output_5_0.png)



```python
import es_core_news_sm
nlp = es_core_news_sm.load()
```


```python
comentario = df['comentario']
normalizado = []
for n in comentario:
    normalizado.append(nlp(str(n)).vector)
columns = ['Col'+str(i) for i in range(1, len(normalizado[0])+1)]
```


```python
mlp = load('../datared/MLPClassifier__all__1567290576136___2_819602.joblib')
```


```python
dummiedf = df.loc[:,['polaridad']]
dummiedf = pd.get_dummies(dummiedf)
y = dummiedf

X = pd.DataFrame(normalizado, columns=columns)
y_pred = mlp.predict(X)
```


```python
confusion_matrix_negative = pd.crosstab(y2[0], y_pred2[0], rownames=['Actual'], colnames=['Predicted'])
confusion_matrix_negative
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Predicted</th>
      <th>0</th>
      <th>1</th>
    </tr>
    <tr>
      <th>Actual</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1981</td>
      <td>104</td>
    </tr>
    <tr>
      <th>1</th>
      <td>119</td>
      <td>794</td>
    </tr>
  </tbody>
</table>
</div>




```python
confusion_matrix_neutral = pd.crosstab(y2[1], y_pred2[1], rownames=['Actual'], colnames=['Predicted'])
confusion_matrix_neutral
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Predicted</th>
      <th>0</th>
      <th>1</th>
    </tr>
    <tr>
      <th>Actual</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>825</td>
      <td>120</td>
    </tr>
    <tr>
      <th>1</th>
      <td>117</td>
      <td>1936</td>
    </tr>
  </tbody>
</table>
</div>




```python
confusion_matrix_positive = pd.crosstab(y2[2], y_pred2[2], rownames=['Actual'], colnames=['Predicted'])
confusion_matrix_positive
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Predicted</th>
      <th>0</th>
      <th>1</th>
    </tr>
    <tr>
      <th>Actual</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2963</td>
      <td>3</td>
    </tr>
    <tr>
      <th>1</th>
      <td>6</td>
      <td>26</td>
    </tr>
  </tbody>
</table>
</div>




```python
plot = df['polaridad'].value_counts().plot(kind='pie', autopct='%.2f', figsize=(12, 12),title='Reacciones Twittes')
```


![png](output_13_0.png)



```python
params = mlp.get_params()
params
```




    {'activation': 'relu',
     'alpha': 0.0001,
     'batch_size': 'auto',
     'beta_1': 0.9,
     'beta_2': 0.999,
     'early_stopping': False,
     'epsilon': 1e-08,
     'hidden_layer_sizes': (79, 13),
     'learning_rate': 'constant',
     'learning_rate_init': 0.001,
     'max_iter': 5000000,
     'momentum': 0.9,
     'n_iter_no_change': 10,
     'nesterovs_momentum': True,
     'power_t': 0.5,
     'random_state': None,
     'shuffle': True,
     'solver': 'lbfgs',
     'tol': 0.0001,
     'validation_fraction': 0.1,
     'verbose': False,
     'warm_start': False}




```python

```
