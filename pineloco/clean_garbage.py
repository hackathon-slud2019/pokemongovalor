import re
import sys
import string

in_file = sys.argv[1]
# in_file = r'muestraParticipantes_filter.csv'
clean_file = open(r'clean_file.txt', "a")

def clean_special_chars(line):
    filter_chars = string.punctuation.replace("|", '').replace('@', '').replace('#', '') + '¿' +'¡'
    chars = re.escape(filter_chars)
    return re.sub(r'['+chars+']', '',line)
    
def clean_url(line):
    new_line = line.replace("|", "|;;").split('|')
    complement = new_line[1] if len(new_line)>1 else '' 
    return re.sub(r'http\S+', '', new_line[0]) + complement.replace(";;", "|")
    
with open(in_file, "r") as f:
    for line in f.readlines():
        clean_file.write(clean_special_chars(clean_url(line)))
    f.close
    clean_file.close()
