import tweepy
from _config import *

consumer_key = CUSTOMER_KEY
consumer_secret = CUSTOMER_SECRET

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

api = tweepy.API(auth)

""" Realiza busqueda y empaqueta vector con resultados """
for tweet in tweepy.Cursor(api.search, q='HackathonSLUD2019').items(10):
    print(tweet.text)